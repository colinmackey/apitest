const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/clubs/users";
const url = new URL(`${config.server}${endpoint}`);
const usersJSON = require('../../../responseJSON/v2/clubs/users.json');

describe('v2/clubs/users endpoint', function() {

	describe('GET', function() {
		var responseJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

		test('should return an object with the correct keys', function() {
			expect(util.deepObjectCompareKeys(responseJSON, usersJSON)).toBeTruthy();
		});

	});

});
