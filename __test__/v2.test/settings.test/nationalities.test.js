const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/settings/nationalities";
const url = new URL(`${config.server}${endpoint}`);
const nationalitiesJSON = require('../../../responseJSON/v2/settings/countries.json');

describe('v2/settings/nationalities endpoint', function() {

	describe('GET', function() {
		var responseJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

		test('should return an object with the correct keys', function() {
			expect(util.deepObjectCompareKeys(responseJSON, nationalitiesJSON)).toBeTruthy();
		});

	});

});
