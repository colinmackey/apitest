const fetch = require('node-fetch');
const config = require('../../../../config');
const util = require('../../../../util');
const endpoint = "/api/v2/oauth/clubs/distance";
const url = new URL(`${config.server}${endpoint}`);
const distanceJSON = require('../../../../responseJSON/v2/clubs/oath/distance.json');

describe('v2slim/performance/distance endpoint', function() {

  	describe('GET', function() {

		var responseJSON = null;
		var responseCourseJSON = null;
		var responseHoleJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
			  	url,
			  	config.options
			)
			.then(response => response.json())
			.then(response => {
			  	return response;
			})
			.catch((error) => {
			  	console.log(error);
			});

		  	const params = {
			    courseID: 1
		  	};
		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseCourseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params1 = {
				holeNumber: 1
			};
			Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

			responseHoleJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

	  	test('should return the correct object keys', function() {
        	expect(util.deepObjectCompareKeys(responseJSON, distanceJSON)).toBeTruthy();
	  	});

  	});

});
