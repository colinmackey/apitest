const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/performance/clubs";
const url = new URL(`${config.server}${endpoint}`);
const clubsJSON = require('../../../responseJSON/v2/performance/clubs.json');

describe('v2/performance/clubs endpoint', function() {

  describe('GET', function() {

    var responseJSON = null;
	var responseCourseJSON = null;
	var responseHoleJSON = null;

    beforeAll(async () => {
      responseJSON = await fetch(
        url,
        config.options
      )
      .then(response => response.json())
      .then(response => {
        return response;
      })
      .catch((error) => {
        console.log(error);
      });

	  const params = {
		  courseID: 1
	  };
	  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

	  responseCourseJSON = await fetch(
		  url,
		  config.options
	  )
	  .then(response => response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });

	  const params1 = {
		  holeNumber: 1
	  };
	  Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

	  responseHoleJSON = await fetch(
		  url,
		  config.options
	  )
	  .then(response => response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });
    });

    test('should return an object with the correct keys', function() {
		expect(util.deepObjectCompareKeys(responseJSON, clubsJSON)).toBeTruthy();
    });

    describe('v2/performance/clubs/{courseID} endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseCourseJSON, clubsJSON)).toBeTruthy();
      });

    });

    describe('v2/performance/clubs/{courseID}/{holeNumber} endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseHoleJSON, clubsJSON)).toBeTruthy();
      });

    });

  });

});
