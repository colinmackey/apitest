const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/performance/puttingv1";
const url = new URL(`${config.server}${endpoint}`);
const puttingv1JSON = require('../../../responseJSON/v2/performance/puttingv1.json');

describe('v2/performance/puttingv1 endpoint', function() {

  	describe('GET', function() {

		var responseJSON = null;
		var responseCourseJSON = null;
		var responseHoleJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
			  	url,
			  	config.options
			)
			.then(response => response.json()
			)
			.then(response => {
			  	return response;
			})
			.catch((error) => {
			  	console.log(error);
			});

		  	const params = {
			    courseID: 1
		  	};
		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseCourseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params1 = {
				holeNumber: 1
			};
			Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

			responseHoleJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

	  	test('should return the correct object keys', function() {
        	expect(util.deepObjectCompareKeys(responseJSON, puttingv1JSON)).toBeTruthy();
	  	});

		describe('v2/performance/puttingv1/{courseID} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseCourseJSON, puttingv1JSON)).toBeTruthy();
			});

		});

		describe('v2/performance/puttingv1/{courseID}/{holeNumber} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseHoleJSON, puttingv1JSON)).toBeTruthy();
			});

		});

  	});

});
