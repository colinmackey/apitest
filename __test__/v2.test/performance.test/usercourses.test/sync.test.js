const fetch = require('node-fetch');
const config = require('../../../../config');
const endpoint = "/api/v2/courses/userscourses/sync";
const util = require('../../../../util');
const url = new URL(`${config.server}${endpoint}`);
const syncJSON = require('../../../../responseJSON/v2/usercourses/sync.json');

async function fetchSync(modifiedSinceDateString) {

  const params = {
    modifiedSince: modifiedSinceDateString
  };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(
    url,
    config.options
  )
  .then(response => response.json())
  .then(response => {
    return response;
  })
  .catch((error) => {
    console.log(error);
  });

}

describe('v2/courses/userscourses/sync?modifiedSince={modifiedSince}&version={version} endpoint', function() {

  	describe('GET', function() {
	    var responseJSON1 = null;
	    var responseJSON2 = null;
	    var dateParam1 = "2018-01-28T02:03:04.005";
	    var dateParam2 = "2018-12-28T02:03:04.005";

	    beforeAll(async () => {
			responseJSON1 = await fetchSync(dateParam1);
			responseJSON2 = await fetchSync(dateParam2);
	  	});

		test('returns an object with the correct keys', function() {
        	expect(util.deepObjectCompareKeys(responseJSON1, syncJSON)).toBeTruthy();
		});

	    test('courseData should be null for for courses updated or added to the user before the modifiedSince parameter', function() {
				/*
		      for (var i = 0; i < responseJSON1.userCourses.length; i++) {
		        if (Date.parse(dateParam1) < Date.parse(responseJSON1.lastChangeTime) || Date.parse(dateParam1) < Date.parse(responseJSON1.userCourses[i].lastUpdateDate)) {
		          expect(responseJSON1.userCourses[i].courseData).toBeTruthy();
		        }
		        else {
		          expect(responseJSON1.userCourses[i].courseData).toBeNull();
		        }
		      }

		      for (var i = 0; i < responseJSON2.userCourses.length; i++) {
		        if (Date.parse(dateParam2) < Date.parse(responseJSON2.lastChangeTime) || Date.parse(dateParam2) < Date.parse(responseJSON2.userCourses[i].lastUpdateDate)) {
		          expect(responseJSON2.userCourses[i].courseData).toBeTruthy();
		        }
		        else {
		          expect(responseJSON2.userCourses[i].courseData).toBeNull();
		        }
		      }
		    });
			*/

	  	});

	});
});
