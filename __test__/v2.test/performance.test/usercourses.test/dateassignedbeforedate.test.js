const fetch = require('node-fetch');
const config = require('../../../../config');
const endpoint = "/api/v2/courses/userscourses/dateassignedbeforedate";
const url = new URL(`${config.server}${endpoint}`);

async function fetchDateassignedbeforedate(date) {

  const params = {
    date: date
  };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(
    url,
    config.options
  )
  .then(response => response.json())
  .then(response => {
    return response;
  })
  .catch((error) => {
    console.log(error);
  });

}

describe('v2/courses/userscourses/sync?modifiedSince={modifiedSince}&version={version} endpoint', function() {

  	describe('GET', function() {
	    var responseJSON = null;

	    beforeAll(async () => {
			responseJSON = await fetchDateassignedbeforedate(new Date(Date.now()).toJSON());
	  	});

		test('response type is boolean', function() {
        	expect(typeof responseJSON).toEqual('boolean');
		});

	    test('courseData should be null for for courses updated or added to the user before the modifiedSince parameter', function() {
				/*
		      for (var i = 0; i < responseJSON1.userCourses.length; i++) {
		        if (Date.parse(dateParam1) < Date.parse(responseJSON1.lastChangeTime) || Date.parse(dateParam1) < Date.parse(responseJSON1.userCourses[i].lastUpdateDate)) {
		          expect(responseJSON1.userCourses[i].courseData).toBeTruthy();
		        }
		        else {
		          expect(responseJSON1.userCourses[i].courseData).toBeNull();
		        }
		      }

		      for (var i = 0; i < responseJSON2.userCourses.length; i++) {
		        if (Date.parse(dateParam2) < Date.parse(responseJSON2.lastChangeTime) || Date.parse(dateParam2) < Date.parse(responseJSON2.userCourses[i].lastUpdateDate)) {
		          expect(responseJSON2.userCourses[i].courseData).toBeTruthy();
		        }
		        else {
		          expect(responseJSON2.userCourses[i].courseData).toBeNull();
		        }
		      }
		    });
			*/

	  	});

	});
});
