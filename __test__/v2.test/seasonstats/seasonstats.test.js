const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const seasonYear = 2015;
const endpoint = "/api/v2/seasonstats/";
const endpoint1 = "/api/v2/seasonstats/" + seasonYear;
const url = new URL(`${config.server}${endpoint}`);
const url1 = new URL(`${config.server}${endpoint1}`);
const seasonstatsJSON = require('../../../responseJSON/v2/seasonstats/seasonstats.json');
const seasonstatsyearJSON = require('../../../responseJSON/v2/seasonstats/seasonstatsyear.json');

describe('v2/seasonstats endpoint', function() {

  describe('GET', function() {

	var responseJSON = null;
    var responseJSON1 = null;

    beforeAll(async () => {
		responseJSON = await fetch(
			url,
			config.options
		)
		.then(response =>  response.json())
		.then(response => {
			return response;
		})
		.catch((error) => {
			console.log(error);
		});

	  responseJSON1 = await fetch(
		  url1,
		  config.options
	  )
	  .then(response =>  response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });
    });

	test('should return an object with the correct keys', function() {
		expect(util.deepObjectCompareKeys(responseJSON, seasonstatsJSON)).toBeTruthy();
	});

    describe('v2/seasonstats/{seasonYear} endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseJSON1, seasonstatsyearJSON)).toBeTruthy();
      });

    });

  });

});
