const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/rounds/roundsplayed";
const url = new URL(`${config.server}${endpoint}`);
const roundsplayedJSON = require('../../../responseJSON/v2/rounds/roundsplayed.json');

describe('v2/roundsplayed endpoint', function() {

  describe('GET', function() {

    var responseJSON = null;

    beforeAll(async () => {
	  responseCourseJSON = await fetch(
		  url,
		  config.options
	  )
	  .then(response =>  response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });
    });

    describe('v2/rounds/roundsplayed endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseCourseJSON, roundsplayedJSON)).toBeTruthy();
      });

    });

  });

});
