const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2/rounds/heatmap";
const url = new URL(`${config.server}${endpoint}`);
const heatmapJSON = require('../../../responseJSON/v2/rounds/heatmap.json');

describe('v2/heatmap endpoint', function() {

  describe('GET', function() {

    var responseJSON = null;

    beforeAll(async () => {
	  responseCourseJSON = await fetch(
		  url,
		  config.options
	  )
	  .then(response =>  response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });
    });

    describe('v2/rounds/heatmap endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseCourseJSON, heatmapJSON)).toBeTruthy();
      });

    });

  });

});
