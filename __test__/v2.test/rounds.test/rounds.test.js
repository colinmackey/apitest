const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const roundID = 158599;
const endpoint = "/api/v2/rounds/" + roundID;
const url = new URL(`${config.server}${endpoint}`);
const roundsJSON = require('../../../responseJSON/v2/rounds/rounds.json');

describe('v2/rounds endpoint', function() {

  describe('GET', function() {

    var responseJSON = null;

    beforeAll(async () => {
	  responseCourseJSON = await fetch(
		  url,
		  config.options
	  )
	  .then(response =>  response.json())
	  .then(response => {
		  return response;
	  })
	  .catch((error) => {
		  console.log(error);
	  });
    });

    describe('v2/rounds/{id} endpoint', function() {

      test('should return an object with the correct keys', function() {
		  expect(util.deepObjectCompareKeys(responseCourseJSON, roundsJSON)).toBeTruthy();
      });

    });

  });

});
