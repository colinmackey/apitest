const fetch = require('node-fetch');
const request = require('request-promise');
const config = require('../../config');
const clubJSON = require('./clubs.json');
const registerEndpoint = "/api/v2/Authentication/RegisterAndValidatePlayerV2";
const deleteUserEndpoint = "/api/Admin/DeleteStressTestUserViaEmail";
const addClubEndpoint = "/api/v2/clubs/usersadd";
const getClubsEndpoint = "/api/v2/clubs/users";
const registerUrl = new URL(`${config.server}${registerEndpoint}`);
const deleteUrl = new URL(`${config.server}${deleteUserEndpoint}`);
const addClubUrl = new URL(`${config.server}${addClubEndpoint}`);
const getClubsUrl = new URL(`${config.server}${getClubsEndpoint}`);

function S4() {
	return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

describe("chris' stress test", async () => {

	var authToken;
	var player;
	var clubFetch;

	//Setup (runs once)
	beforeAll(async (done) => {
		const guid = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
		player = {
		    Email: "ST_" + guid + "@shotscope.com",
		    Password: "Qazxsw2!",
		    ConfirmPassword: "Qazxsw2!",
		    HomeCourseID: 1,
		    PlayerType: "Amateur",
		    Gender: "Male",
		    Handicap: 0,
		    Dob: new Date(1981, 1, 1),
		    FirstName: "Bill",
		    LastName: "Ben"
		};
		var playerJSON = JSON.stringify(player);
		var response;

		var loginData = {
			grant_type: 'password',
			username: player.Email,
			password: player.Password
		};

		//Register user
		var response2 = await fetch(
	        registerUrl,
	        {
	        	method: "POST",
				body: playerJSON,
				headers: {
			      Accept: "application/json",
			      "Content-Type": "application/json"
			    }
	        }
    	)
	    .then(response => {
			return response;
	    })
	    .catch((error) => {
	        console.log(error);
	    });
	    console.log("Creating user: " + response2.statusText);

		//Log in using node request because I can't get it to work with fetch
		var options = { method: 'POST',
		  url: 'https://staging-dashboard.shotscope.com/Token',
		  headers:
		   {
		     'Cache-Control': 'no-cache',
		     'Content-Type': 'application/x-www-form-urlencoded'
		 },
		  form: loginData
		};

		authToken = await request(options, function (error, response, body) {
  			console.log("Retrieving token: " + response.statusMessage);
		})
		.then(response => JSON.parse(response))
		.then(response => {
			return response.access_token;
	    });
		
		await fetch(
			addClubUrl,
			{
				method: "POST",
				body: JSON.stringify({
					"Type": "Driver",
					"Make": "Default",
					"Model": "Default",
					"ClubName": "Driver",
					"Colour": "0",
					"Assigned": "D",
					"AssignmentDate": null
				}),
				headers: {
				  Accept: "application/json",
				  "Content-Type": "application/json",
				  Authorization: 'Bearer ' + authToken
				}
			}
		)
		.then(response => {
			return response;
		})
		.catch((error) => {
			console.log(error);
		});

		clubFetch = await fetch(
		  	getClubsUrl,
		  	{
		  		method: "GET",
				headers: {
				  Accept: "application/json",
				  "Content-Type": "application/json",
				  Authorization: 'Bearer ' + authToken
				}
	  		}
		)
		.then(response => response.json())
		.then(response => {
		  return response;
		})
		.catch((error) => {
		  console.log(error);
		});

		done();
	});

	//Teardown (runs once)
	afterAll(async() => {
		const params = {
			email: player.Email
		};
		Object.keys(params).forEach(key => deleteUrl.searchParams.append(key, params[key]));

		var response = await fetch(
			deleteUrl,
			config.options
		)
		.then(response => {
			return response;
		})
		.catch((error) => {
			console.log(error);
		});
	    console.log("deleting user: " + response.statusText);
	});

	test('clubs from clubJSON should be on the account', () => {

		expect(clubFetch[0]).toEqual(expect.objectContaining(clubJSON.clubs[0]));

	});

});
