const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2slim/performance/putting";
const url = new URL(`${config.server}${endpoint}`);
const puttingJSON = require('../../../responseJSON/v2slim/performance/putting.json');

describe('v2slim/performance/putting endpoint', function() {

  	describe('GET', function() {

		var responseJSON = null;
		var responseCourseJSON = null;
		var responseHoleJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
			  	url,
			  	config.options
			)
			.then(response => response.json())
			.then(response => {
			  	return response;
			})
			.catch((error) => {
			  	console.log(error);
			});

		  	const params = {
			    courseID: 1
		  	};
		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseCourseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params1 = {
				holeNumber: 1
			};
			Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

			responseHoleJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

	  	test('should return the correct object keys', function() {
        	expect(util.deepObjectCompareKeys(responseJSON, puttingJSON)).toBeTruthy();
	  	});

		describe('v2slim/performance/putting/{courseID} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseCourseJSON, puttingJSON)).toBeTruthy();
			});

		});

		describe('v2slim/performance/putting/{courseID}/{holeNumber} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseHoleJSON, puttingJSON)).toBeTruthy();
			});

		});

  	});

});
