const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2slim/performance/teeshots";
const url = new URL(`${config.server}${endpoint}`);
const teeshotsJSON = require('../../../responseJSON/v2slim/performance/teeshots.json');

describe('v2slim/performance/teeshots endpoint', function() {

  	describe('GET', function() {

		var responseJSON = null;
		var responseCourseJSON = null;
		var responseHoleJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
			  	url,
			  	config.options
			)
			.then(response => response.json())
			.then(response => {
			  	return response;
			})
			.catch((error) => {
			  	console.log(error);
			});

		  	const params = {
			    courseID: 1
		  	};
		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseCourseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params1 = {
				holeNumber: 1
			};
			Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

			responseHoleJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

	  	test('should return the correct object keys', function() {
        	expect(util.deepObjectCompareKeys(responseJSON, teeshotsJSON)).toBeTruthy();
	  	});

		describe('v2slim/performance/teeshots/{courseID} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseCourseJSON, teeshotsJSON)).toBeTruthy();
			});

		});

		describe('v2slim/performance/teeshots/{courseID}/{holeNumber} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseHoleJSON, teeshotsJSON)).toBeTruthy();
			});

		});

  	});

});
