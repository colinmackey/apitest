const fetch = require('node-fetch');
const config = require('../../../config');
const util = require('../../../util');
const endpoint = "/api/v2mobile/performance/approaches";
const url = new URL(`${config.server}${endpoint}`);
const approachesJSON = require('../../../responseJSON/v2mobile/performance/approaches.json');

describe('v2mobile/performance/approaches endpoint', function() {

	describe('GET', function() {
		var responseJSON = null;
		var responseCourseJSON = null;
		var responseHoleJSON = null;

		beforeAll(async () => {
			responseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params = {
			    courseID: 1
		  	};
		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseCourseJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			const params1 = {
				holeNumber: 1
			};
			Object.keys(params1).forEach(key => url.searchParams.append(key, params1[key]));

			responseHoleJSON = await fetch(
				url,
				config.options
			)
			.then(response => response.json())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

		test('should return an object with the correct keys', function() {
			expect(util.deepObjectCompareKeys(responseJSON, approachesJSON)).toBeTruthy();
		});

		describe('v2mobile/performance/approaches/{courseID} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseCourseJSON, approachesJSON)).toBeTruthy();
			});

		});

		describe('v2mobile/performance/approaches/{courseID}/{holeNumber} endpoint', function() {

			test('should return the correct object keys', function() {
				expect(util.deepObjectCompareKeys(responseHoleJSON, approachesJSON)).toBeTruthy();
			});

		});

	});

});
