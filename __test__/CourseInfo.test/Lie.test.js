const fetch = require('node-fetch');
const config = require('../../config');
const util = require('../../util');
const endpoint = "/api/CourseInfo/Lie";
const url = new URL(`${config.server}${endpoint}`);

describe('CourseInfo/Lie?lat={lat}&lng={lng}&courseID={courseID} endpoint', function() {

  	describe('GET', function() {

		var responseRough = null;
		var responseTee = null;
		var responseGreen = null;
		var responseFairway = null;

		beforeAll(async () => {

			const paramsRough = {
				lat: 37.420260784969443,
				lng: -122.03828105633511,
			    courseID: 12736
		  	};
			const paramsTee = {
				lat: 37.418934523809519,
				lng: -122.03982428571429,
				courseID: 12736
			};
			const paramsGreen = {
				lat: 37.421132362674221,
				lng: -122.03709024934126,
				courseID: 12736
			};
			const paramsFairway = {
				lat: 37.425634333333335,
				lng: -122.03738333333334,
				courseID: 12736
			};

		    Object.keys(paramsRough).forEach(key => url.searchParams.append(key, paramsRough[key]));

			responseRough = await fetch(
			  	url,
			  	config.options
			)
			.then(response => response.text())
			.then(response => {
				return response;
			})
			.catch((error) => {
			  	console.log(error);
			});

			for (var key in paramsRough) {
				url.searchParams.delete(key);
			}
			Object.keys(paramsTee).forEach(key => url.searchParams.append(key, paramsTee[key]));

			responseTee = await fetch(
				url,
				config.options
			)
			.then(response => response.text())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			for (var key in paramsTee) {
				url.searchParams.delete(key);
			}
			Object.keys(paramsGreen).forEach(key => url.searchParams.append(key, paramsGreen[key]));

			responseGreen = await fetch(
				url,
				config.options
			)
			.then(response => response.text())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});

			for (var key in paramsGreen) {
				url.searchParams.delete(key);
			}
			Object.keys(paramsFairway).forEach(key => url.searchParams.append(key, paramsFairway[key]));

			responseFairway = await fetch(
				url,
				config.options
			)
			.then(response => response.text())
			.then(response => {
				return response;
			})
			.catch((error) => {
				console.log(error);
			});
		});

		//TODO: test for bunker lie type

	  	test('should return "Rough"', function() {
        	expect(responseRough).toEqual('Rough');
	  	});

		test('should return "tee"', function() {
			expect(responseTee).toEqual('tee');
		});

		test('should return "green"', function() {
			expect(responseGreen).toEqual('green');
		});

		test('should return "Fairway"', function() {
			expect(responseFairway).toEqual('fairway');
		});

  	});

});
