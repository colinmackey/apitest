const fetch = require('node-fetch');
const config = require('../../config');
const util = require('../../util');
const endpoint = "/api/EditRound/tees";
const url = new URL(`${config.server}${endpoint}`);
const teesJSON = require('../../responseJSON/EditRound/tees.json');

describe('EditRound/tees?roundID={roundID} endpoint', function() {

  	describe('GET', function() {

		var responseJSON = null;

		beforeAll(async () => {

			const params = {
				roundID: 158597
		  	};

		    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

			responseJSON = await fetch(
			  	url,
				{
				  method: "GET",
				  headers: {
				    Accept: "text/plain; charset=utf-16",
				    "Content-Type": "text/plain; charset=utf-16",
				    Authorization: 'Bearer ' + config.token
				  }
				}
			)
			.then(response => {
				return response.json()
			})
			.then(response => {
				return response;
			})
			.catch((error) => {
			  	console.log(error);
			});
		});

	  	test('should return the correct object keys', function() {
			expect(util.deepObjectCompareKeys(responseJSON, teesJSON)).toBeTruthy();
	  	});
  	});

});
