This test suite is designed for black-box testing the web API using the documented responses as templates for expected outputs.
Enter a valid admin login token into the config or authorized endpoints will fail
