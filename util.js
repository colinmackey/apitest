//This function assumes that arrays will contain objects with similar keys
 var deepObjectCompareKeys = function(actual, expected) {
	if (!Array.isArray(actual)) {
		for(var prop in actual) {
			// If one of the props is an array of objects then look inside it for more props recursively
			if (Array.isArray(actual[prop]) && typeof actual[prop][0] == 'object')
			{
				if (!expected.hasOwnProperty(prop)) {
					console.log("expected response does not contain the actual prop:" + prop);
					return false;
				}
				else if(!deepObjectCompareKeys(actual[prop][0], expected[prop][0]))
					return false;
			}
			//Check if b has the prop and same number of props
			 else if (!expected.hasOwnProperty(prop) || (Object.keys(actual).length) !== Object.keys(expected).length) {
				//log the diff in keys if they don't match
				console.log("------------------actual response------------------\n" + Object.keys(actual) + "\n------------------actual response end------------------");
				console.log("------------------expected response------------------\n" + Object.keys(expected) + "\n------------------expected response end------------------");
				return false;
			}
		}
	}
	else if (typeof actual[0] == 'object') {
		if(!deepObjectCompareKeys(actual[0], expected[0]))
			return false;
		}
	return true;
}

module.exports.deepObjectCompareKeys = deepObjectCompareKeys;
